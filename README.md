# necis - new computer interface system

necis is a GUI concept for interacting with computers.

## Core concepts

### One infrastructure for mobile and desktop

Applications written for necis should be written independent of screen sizes.
necis should take care of displaying the application corretly instead.
All the applications need to provide are a list of GUI elements for the current
main content, together with navigation items and actions. necis will then
provide the necessary controls so that users can interact with those items.
Naturally the controls will be different between common desktop computers
that can be controlled via moust and keyboard and smartphones that are
mostly controlled via touch gestures.

Accessibility shall be a core feature of necis so that alternative methods
of controlling applications like alternative keyboard and touch controls and
speech output can be used.


### Contex-based interfaces for applications

A concept of necis is to define context-based interfaces so that applications
can plug into core functionality of necis. Applications that generate
notifications are grouped into contexts, so that users can get notifications
only for the context they request:

- The traffic context would show news from applications that retrieve
  information about nearby road constructions, traffic jams or train or bus
  service information.

- Weather and alert applications can plug into the wheather context and provide
  current weather information together with a forecast and weather warnings.

- The news context would aggregate RSS feeds and from news applications.

- The calendar context would show a calendar and todo lists where all
  applications that can provide such information can be plugged into.

- The personal messages context would display the amount of new messages
  received via E-Mail or a messenger, grouped into several user-defined
  categories at your option.

- There could also be combined contextes like "work" or "hobbies" that can
  consist of combination of base contextes. For those, you may want a dashboard
  that shows the amount of new messages, project-related activity and maybe
  some project-specific news. Since it all depends on the kind of work you
  are doing, these combined contextes can consist of all kinds of basic
  contextes: A delivery worker may need the traffic context in it while an
  office worker may also need a calendar. necis could provide basic templates
  for different kinds of work when setting it up.


### Unified GUI

necis applications shall focus on the part of displaying content to the user
and processing user input while necis takes care of displaying navigation
and action items of the running applictions.

The following picture shows the GUI concept of necis for desktop computers.
The colours in the picture are meant to distinct the different areas of the GUI
and not as a definition of the standard colour scheme.

![The GUI concept of necis: Navigation items at the top, actions on the left side, system tray and task list at the bottom. Navigation is controlled via function keys, actions via CRTL and a function key.](necis-gui-concept.svg)
